# How to execute the code in Spark
- Clone the docker: https://gitlab.com/alecy/docker_hadoop_spark
- Start the docker
```bash
$ cd FILE_DOCKER
$ docker-compose up
```
- Copy all the files from directory to namenode
- Inside namenode copy files from directory to HDFS:
```bash
$ hdfs dfs -mkdir -p input
$ hdfs dfs -put ./PATH_TEXT_FILES/* input
```
- Execute the program with python version 3.7 and the parameters size_gram (size of the gram) and minimum_frequency (the minimum frequency of the gram):
```bash
$ spark-submit --executor-memory 4G --master spark://localhost:7077 --conf spark.eventLog.enabled=true --conf spark.eventLog.dir=hdfs://localhost:9000/ggvd/logs --conf spark.history.fs.logDirectory=hdfs://localhost:9000/ggvd/logs mapreduce-spark.py --size_gram 3 --minimum_frequency 4 --input_directory '/PATH/input/' --output_directory 'hdfs://localhost:9000/PATH/output'
```
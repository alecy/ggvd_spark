import re
import argparse
import os
from pyspark.context import SparkContext
from pyspark.sql.session import SparkSession
from pyspark.sql.types import Row
from pyspark.ml.feature import NGram
from pyspark.sql import SQLContext
from pyspark.storagelevel import StorageLevel 
from hdfs import InsecureClient

client = InsecureClient("http://localhost:9870")

sc = SparkContext("local", "test")

sqlContext = SQLContext(sc)

spark = SparkSession\
        .builder\
        .appName("PythonMapReducer")\
        .getOrCreate()

parser = argparse.ArgumentParser()
parser.add_argument("--size_gram")
parser.add_argument("--minimum_frequency")
parser.add_argument("--input_directory")
parser.add_argument("--output_directory")
args = parser.parse_args()
n_gram = int(args.size_gram)
min_freq = int(args.minimum_frequency)
directory = args.input_directory
output_directory = args.output_directory

pre = sc.emptyRDD()

for filename in client.list(f"{directory}"):
    textFile = sc.textFile(f"hdfs://localhost:9000/{directory}/{filename}").persist(StorageLevel.MEMORY_ONLY)

    words = textFile.flatMap(lambda line: line.split("\t"))\
                .map(lambda line: re.sub(r"[^\w\s]", r"", line))\
                .map(lambda line: line.split(" "))\
                .map(lambda x: filter(None, x))\
                .flatMap(lambda x:x).persist(StorageLevel.MEMORY_ONLY)

    df = spark.createDataFrame([Row(words = words.collect())])
    ngram = NGram(n=n_gram)
    ngram.setInputCol("words")
    ngram.setOutputCol("nGrams")
    ngram_df = ngram.transform(df, {ngram.outputCol: "nGrams"})

    pre_counts_1 = ngram_df.rdd.flatMap(lambda row: row[1]).map(lambda ngram: ((ngram, 1), re.sub(r' ', r'', filename))).persist(StorageLevel.MEMORY_ONLY)

    pre = pre_counts_1.union(pre).persist(StorageLevel.MEMORY_ONLY)

counts = pre.map(lambda x:x[0]).reduceByKey(lambda a,b: (a+b)).persist(StorageLevel.MEMORY_ONLY)

file_name = pre.map(lambda x: (x[0][0], x[1])).reduceByKey(lambda a,b: a if a==b else a+' '+b).persist(StorageLevel.MEMORY_ONLY)

join = counts.join(file_name).persist(StorageLevel.MEMORY_ONLY)

final = join.filter(lambda x: x[1][0] > min_freq)\
            .map(lambda x: (x[0], x[1][0], x[1][1])).persist(StorageLevel.MEMORY_ONLY)

final.saveAsTextFile(output_directory)

spark.stop()